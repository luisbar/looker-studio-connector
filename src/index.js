const createCommunityConnector = DataStudioApp.createCommunityConnector();
const userProperties = PropertiesService.getUserProperties();

/**
 * Mandatory function required by Google Data Studio that should
 * return the authentication method required by the connector
 * to authorize the third-party service.
 * @return {Object} AuthType
 */
function getAuthType() {
  return createCommunityConnector
    .newAuthTypeResponse()
    .setAuthType(createCommunityConnector.AuthType.KEY)
    .setHelpUrl(
      'https://docs.orangepill.cloud/orangepill-api/overview#authorization'
    )
    .build();
}

/**
 * Mandatory function required by Google Data Studio that should
 * clear user credentials for the third-party service.
 * This function does not accept any arguments and
 * the response is empty.
 */
function resetAuth() {
  userProperties.deleteProperty('apiKey');
}

/**
 * Mandatory function required by Google Data Studio that should
 * determine if the authentication for the third-party service is valid.
 * And if you try to bypass it returning true, the connector won't
 * display the form for introducing the credentials for the third-party.
 * @return {Boolean}
 */
function isAuthValid() {
  return checkForValidApiKey(userProperties.getProperty('apiKey'));
}

/**
 * Mandatory function required by Google Data Studio that should
 * set the credentials after the user enters either their
 * credential information on the community connector configuration page.
 * @param {Object} request The set credentials request.
 * @return {object} An object with an errorCode.
 */
function setCredentials(request) {
  const isValidApiKey = checkForValidApiKey(request.key);

  if (!isValidApiKey)
    return {
      errorCode: 'INVALID_CREDENTIALS',
    };

  userProperties.setProperty('apiKey', request.key);
  return {
    errorCode: 'NONE',
  };
}

/**
 * Mandatory function required by Google Data Studio that should
 * return the user configurable options for the connector.
 * @param {Object} request
 * @return {Object} fields
 */
function getConfig() {
  const config = createCommunityConnector.getConfig();

  config
    .newInfo()
    .setId('instructions')
    .setText(
      'Enter the resource you want to fetch, for instance, users, accounts, etc.'
    );

  const users = config.newOptionBuilder().setLabel('Users').setValue('users');
  const transactions = config
    .newOptionBuilder()
    .setLabel('Transactions')
    .setValue('transactions');
  const virtualCurrencies = config
    .newOptionBuilder()
    .setLabel('Virtual Currencies')
    .setValue('currencies');
  const accounts = config
    .newOptionBuilder()
    .setLabel('Accounts')
    .setValue('accounts');

  config
    .newSelectSingle()
    .setId('resource')
    .setName('Resource')
    .setHelpText('Select the resource you want to fetch')
    .setAllowOverride(true)
    .addOption(users)
    .addOption(transactions)
    .addOption(virtualCurrencies)
    .addOption(accounts);

  config.setDateRangeRequired(true);

  return config.build();
}

/**
 * Mandatory function required by Google Data Studio that should
 * return the schema for the given request.
 * This provides the information about how the connector's data is organized.
 * @param {Object} request
 * @return {Object} fields
 */
function getSchema(request) {
  return { schema: getSchemaFields(request).build() };
}

/**
 * Mandatory function required by Google Data Studio that should
 * return the tabular data for the given request.
 * @param {Object} request
 * @return {Object}
 */
function getData(request) {
  try {
    const { dateRange } = request;
    const query = {
      created_at: {
        $gte: Date.parse(new Date(dateRange.startDate).toUTCString()),
        $lte: Date.parse(new Date(dateRange.endDate).toUTCString()),
      },
    };
    const requestedFieldIds = request.fields.map(function (field) {
      return field.name;
    });
    const requestedFields = getSchemaFields(request).forIds(requestedFieldIds);
    const url = `https://api.orangepill.cloud/v1/${
      request.configParams.resource
    }/all?query=${encodeURIComponent(JSON.stringify(query))}&scope=-own,all`;

    const options = {
      method: 'GET',
      headers: {
        'x-api-key': String(userProperties.getProperty('apiKey')), // throws an error if you does not transform it to string
        'Content-Type': 'application/json',
      },
    };

    const response = UrlFetchApp.fetch(url, options);

    if (response.getResponseCode() != 200)
      return createCommunityConnector
        .newUserError()
        .setDebugText(
          `Error fetching data from API. Exception details: ${response}`
        )
        .setText(`Error fetching data from API: ${response.getResponseCode()}`)
        .throwException();

    const parsedResponse = JSON.parse(response.getContentText());
    const rows = responseToRows({
      requestedFields,
      data: parsedResponse,
    });

    return {
      rows,
      schema: requestedFields.build(),
    };
  } catch (error) {
    createCommunityConnector
      .newUserError()
      .setDebugText(`Error fetching data. Exception details: ${error}`)
      .setText(`Error fetching data: ${error.message}`)
      .throwException();
  }
}

/**
 * Should return true if the current user should see debug messages and stack traces.
 * @returns {boolean}
 */
function isAdminUser() {
  const email = Session.getEffectiveUser().getEmail();
  const domain = email.split('@')[1];
  return domain === 'orangepill.cc';
}

function checkForValidApiKey(apiKey) {
  const url = 'https://api.orangepill.cloud/v1/users';
  const options = {
    method: 'GET',
    headers: {
      'x-api-key': String(apiKey), // throws an error if you does not transform it to string
      'Content-Type': 'application/json',
    },
    muteHttpExceptions: true,
  };
  const response = UrlFetchApp.fetch(url, options);

  return response.getResponseCode() == 200;
}

function getSchemaFields(request) {
  const configParams = request.configParams;
  const fields = createCommunityConnector.getFields();
  const types = createCommunityConnector.FieldType;
  const fieldsByResource = {
    users() {
      fields.newDimension().setId('id').setType(types.TEXT);
      fields.newDimension().setId('email').setType(types.TEXT);
      fields.newDimension().setId('username').setType(types.TEXT);
      fields.newDimension().setId('identity').setType(types.TEXT);
      fields.newDimension().setId('deleted').setType(types.BOOLEAN);
      fields
        .newDimension()
        .setId('createdAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);

      return fields;
    },
    accounts() {
      fields.newDimension().setId('id').setType(types.TEXT);
      fields.newDimension().setId('asset').setType(types.TEXT);
      fields.newDimension().setId('chain').setType(types.TEXT);
      fields.newDimension().setId('description').setType(types.TEXT);
      fields.newDimension().setId('holder').setType(types.TEXT);
      fields.newDimension().setId('currency').setType(types.TEXT);
      fields.newDimension().setId('type').setType(types.TEXT);
      fields.newDimension().setId('testnet').setType(types.TEXT);
      fields.newDimension().setId('reference').setType(types.TEXT);
      fields.newDimension().setId('availableBalance').setType(types.NUMBER);
      fields.newDimension().setId('totalBalance').setType(types.NUMBER);
      fields.newDimension().setId('assetBalance').setType(types.NUMBER);
      fields.newDimension().setId('gasBalance').setType(types.NUMBER);
      fields.newDimension().setId('ramp').setType(types.TEXT);
      fields.newDimension().setId('address').setType(types.TEXT);
      fields.newDimension().setId('default').setType(types.BOOLEAN);
      fields.newDimension().setId('active').setType(types.BOOLEAN);
      fields.newDimension().setId('frozen').setType(types.BOOLEAN);
      fields.newDimension().setId('owner').setType(types.TEXT);
      fields.newDimension().setId('alias').setType(types.TEXT);
      fields.newDimension().setId('deleted').setType(types.BOOLEAN);
      fields
        .newDimension()
        .setId('createdAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);
      fields
        .newDimension()
        .setId('updatedAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);
      fields
        .newDimension()
        .setId('deletedAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);

      return fields;
    },
    transactions() {
      fields.newDimension().setId('id').setType(types.TEXT);
      fields.newDimension().setId('sourceAccount').setType(types.TEXT);
      fields.newDimension().setId('sourceAlias').setType(types.TEXT);
      fields.newDimension().setId('sourceHolder').setType(types.TEXT);
      fields.newDimension().setId('destinationAccount').setType(types.TEXT);
      fields.newDimension().setId('destinationAlias').setType(types.TEXT);
      fields.newDimension().setId('destinationHolder').setType(types.TEXT);
      fields.newDimension().setId('value').setType(types.NUMBER);
      fields.newDimension().setId('asset').setType(types.TEXT);
      fields.newDimension().setId('amount').setType(types.NUMBER);
      fields.newDimension().setId('status').setType(types.TEXT);
      fields.newDimension().setId('type').setType(types.TEXT);
      fields.newDimension().setId('reference').setType(types.TEXT);
      fields.newDimension().setId('holder').setType(types.TEXT);
      fields.newDimension().setId('owner').setType(types.TEXT);
      fields.newDimension().setId('description').setType(types.TEXT);
      fields.newDimension().setId('deleted').setType(types.BOOLEAN);
      fields
        .newDimension()
        .setId('createdAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);
      fields
        .newDimension()
        .setId('updatedAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);
      fields
        .newDimension()
        .setId('deletedAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);

      return fields;
    },
    currencies() {
      fields.newDimension().setId('id').setType(types.TEXT);
      fields.newDimension().setId('name').setType(types.TEXT);
      fields.newDimension().setId('supply').setType(types.NUMBER);
      fields.newDimension().setId('base').setType(types.TEXT);
      fields.newDimension().setId('rate').setType(types.NUMBER);
      fields.newDimension().setId('precision').setType(types.NUMBER);
      fields.newDimension().setId('country').setType(types.TEXT);
      fields.newDimension().setId('currency').setType(types.TEXT);
      fields.newDimension().setId('account').setType(types.TEXT);
      fields.newDimension().setId('active').setType(types.BOOLEAN);
      fields.newDimension().setId('deleted').setType(types.BOOLEAN);
      fields
        .newDimension()
        .setId('createdAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);
      fields
        .newDimension()
        .setId('updatedAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);
      fields
        .newDimension()
        .setId('deletedAt')
        .setType(types.YEAR_MONTH_DAY_SECOND);

      return fields;
    },
  };

  return fieldsByResource[configParams.resource]();
}

function responseToRows({ requestedFields, data }) {
  const fieldsAsArrays = requestedFields.asArray();
  return data.map(function (item) {
    const mappedData = fieldsAsArrays.reduce(function (accumulator, field) {
      accumulator.push(
        getDataFields({
          accessor: field?.getId(),
          item,
        })
      );
      return accumulator;
    }, []);

    return { values: mappedData };
  });
}

function getDataFields({ accessor, item }) {
  const fieldByAccessor = {
    id: item.id,
    username: item.username,
    email: item.email,
    identity: item.identity,
    holder: item.holder,
    sourceAccount: item.source?.account,
    sourceAlias: item.source?.alias,
    sourceHolder: item.source?.holder,
    destinationAccount: item.destination?.account,
    destinationAlias: item.destination?.alias,
    destinationHolder: item.destination?.holder,
    value: item.value,
    amount: item.amount,
    asset: item.asset,
    owner: item.owner,
    chain: item.chain,
    currency: item.currency,
    testnet: item.testnet,
    availableBalance: item.balance?.available,
    totalBalance: item.balance?.total,
    assetsBalance: item.balance?.assets,
    gasBalance: item.balance?.gas,
    ramp: item.ramp,
    address: item.address,
    deleted: item.deleted,
    status: item.status,
    createdAt: item.created_at,
    updatedAt: item.updated_at,
    deletedAt: item.deleted_at,
    type: item.type,
    name: item.name,
    supply: item.supply,
    base: item.base,
    rate: item.rate,
    precision: item.precision,
    country: item.country,
    account: item.account,
    active: item.active,
  };

  return fieldByAccessor[accessor];
}
