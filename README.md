## Clasp (Command Line Apps Script Projects)
It allows you to develop your Apps Script projects locally. That means you can check-in your code into source control, collaborate with other developers, and use your favorite tools to develop Apps Script.

- If you want to use clasp, first you have to login using the following command `clasp login`
- This Apps Script project was initialized using the following command
  `clasp create --title looker-studio-connector --type api --rootDir $(pwd)/src`
- The code was pushed to Apps Script using the following command
  `(cd ./src && clasp push)`

## How to test it?
- Go to App Script editor
- Click on deploy, there you will see the option `Test Deployments`, click on it
- It will give you the deployment id, so copy it and replace it on the following url `https://lookerstudio.google.com/datasources/create?connectorId=DEPLOYMENT_ID`
- Open the url on any browser
- Enjoy the connector

## Tips and tricks
- If you face any error you can add logs and you will be able to see it on `Executions` section on App Script editor
- Don't bypass the method `isAuthValid`, because you won't be able to see the credentials form
- If your data source requires date as a parameter, call `config.setDateRangeRequired(true)`
- The `getData` method receives the date range on `dateRange` property and if you don't set a date range component on your report the default date rage will bee 28 days, here you have an example the object gotten by `getData` method
  ```javascript
  {
    "scriptParams": {
      "lastRefresh": "1724415435655"
    },
    "dateRange": {
      "endDate": "2024-08-09",
      "startDate": "2024-08-01"
    },
    "fields": [
      {
        "name": "id"
      },
      {
        "name": "value"
      }
    ],
    "configParams": {
      "resource": "transactions"
    }
  }
  ```
- The `lastRefresh` props gotten on `getData` method can help you to determine whether to make a new fetch request to the data source or serve data from the cache
- [Here](https://developers.google.com/looker-studio/connector/reference#configtype) you can find input types supported by `getConfig` method
- [Here](https://developers.google.com/looker-studio/connector/stepped-configuration) you have info about stepped configuration, it allows a connector to dynamically populate the connector configuration based on user-provided answers. For example, populating a "City" dropdown after a "State" dropdown is selected